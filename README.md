Docker image for Atlassian Bamboo Server with MySQL Connector from Oracle.

# Installation

**Important:** Replace `<desired_version>` with a specific Docker tag

In this case `mnt/storage/atlassian/bamboo` is the mounted path to Bamboo Server.

```sh
docker volume create --name bambooVolume
docker volume create --name bambooInstallVolume
docker run -v bambooVolume:/mnt/storage/atlassian/bamboo -v bambooInstallVolume:/opt/atlassian/bamboo --name="bamboo" --init -d --network host kkfsolutions/bamboo-server-mysql:<desired_version>
```

If you want to use MariaDB or a MySQL version lower than `5.6.3` add this variable to your `docker run`: 
`-e JAVA_OPTS="-Dbamboo.upgrade.fail.if.mysql.unsupported=false"`

If you want to use `https` and run your Bamboo through a proxy add the folowing variables to `bambooInstallVolume/conf/server.xml` inside the `Connector` tag:
```xml
scheme="https" 
secure="true" 
redirectPort="<desired_redirect_port>"
proxyName="<desired_proxy_name>"
proxyPort="<desired_proxy_port>"
```

Examples:
- `<desired_redirect_port>`: `443`
- `<desired_proxy_name>`: `bamboo.example.com`
- `<desired_proxy_port>`: `443`

# Upgrade

To upgrade Bamboo Server just stop the `bamboo` container and create a new one.

```sh
docker stop bamboo
docker rm bamboo
docker pull kkfsolutions/bamboo-server-mysql:<desired_version>
docker run ... (see above)
```

# More Information

See the official [Atlassian Bamboo Server Docker Image](https://hub.docker.com/r/atlassian/bamboo-server) for more information.
